<?php

use App\Http\Controllers\Backend\DashboardController;
use App\Http\Controllers\Backend\NamecheapController;

// All route names are prefixed with 'admin.'.
Route::redirect('/', '/admin/dashboard', 301);
Route::get('dashboard', [DashboardController::class, 'index'])->name('dashboard');

Route::get('/domains/{page?}','NamecheapController@index')->name('index');
Route::get('/domain/create','NamecheapController@create')->name('create');
Route::post('/save-domain','NamecheapController@saveDomain')->name('saveDomain');
Route::get('/namecheap','NamecheapController@namecheap');
Route::get('/getDNS','NamecheapController@getDNS')->name('getDNS');
Route::post('/updateDNS','NamecheapController@updateDNS')->name('updateDNS');
Route::get('/newDomains','NamecheapController@newDomains')->name('newDomains');
Route::get('/createCloudflare','NamecheapController@createCloudflare')->name('createCloudflare');

Route::get('/createPleskDomain','NamecheapController@createPleskDomain')->name('createPleskDomain');

Route::get('/createTrafficArmorCampaign','NamecheapController@createTrafficArmorCampaign')->name('createTrafficArmorCampaign');

Route::get('/updateFilesToPlesk','NamecheapController@updateFilesToPlesk')->name('updateFilesToPlesk');