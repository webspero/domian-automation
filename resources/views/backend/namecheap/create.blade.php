<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
        <!-- Styles -->
        <style>
            .content {
                width: 100%;
                text-align: center;
                position: relative;
                display: inline-block;
            }
            .content form{
              width: 30%;
              margin: auto;
              padding-top: 200px;
            }
        </style>
    </head>
    <body>
    @if(session()->has('message.level'))
        <div class="alert alert-{{ session('message.level') }}"> 
        {!! session('message.content') !!}
        </div>
    @endif
        <div class="flex-center position-ref full-height">
            <div class="content">
                <form method="POST" action="{{route('admin.saveDomain')}}">
                  @csrf
                  <div class="form-group">
                    <h3><label for="create-domain">Create Domain</label></h3>
                    <input type="text" name="domain" class="form-control" id="create-domain" maxlength="191" value="" placeholder="Enter a domain name">
                  </div>
                  <button type="submit" class="btn btn-primary">Create</button>
                </form> 
            </div>
        </div>
        <script>
          $(document).ready(function(){
              setTimeout(function() {
                  $(".alert-success").hide(); 
              }, 5000);
          });
        </script> 
    </body>
</html>
