
@extends('backend.layouts.app')

@section('title', app_name() . ' | ' . __('labels.backend.access.users.management'))

@section('content')
<style>
  
  .alert {
    margin-bottom: 0 !important; 
  }
  .main .container-fluid {
    padding: 0 !important;
  }
  .button-success-table, .update_dns button {
    padding: 5px;
    font-size: 10px;
  }
  .col {
    text-align: center;
    padding: 0px !important;
  }
</style>
<div class="card">
    <div class="card-body">
        <div class="row">
            <div class="col-sm-5">
                <h4 class="card-title mb-0">
                    All Domains
                </h4>
            </div><!--col-->
        </div><!--row-->
        <div class="row mt-4">
            <div class="col">
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Id</th>
                            <th>Name</th>
                            <th>Created</th>
                            <th>Expires</th>
                            <th>IsExpired</th>
                            <th>IsLocked</th>
                            <th>AutoRenew</th>
                            <th>WhoisGuard</th>
                            <th>Action</th>
                            <th>IsPremium</th>
                            <th>IsOurDNS</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($domains['CommandResponse']['DomainGetListResult']['Domain'] as $key=>$domain)
                            <tr>
                                <td>{{$key+1}}</td>
                                <td>{{ $domain['@attributes']['ID'] }}</td>
                                <td>{{ $domain['@attributes']['Name'] }}</td>
                                <td>{{ $domain['@attributes']['Created'] }}</td>
                                <td>{{ $domain['@attributes']['Expires'] }}</td>
                                <td>{{ $domain['@attributes']['IsExpired'] }}</td>
                                <td>{{ $domain['@attributes']['IsLocked'] }}</td>
                                <td>{{ $domain['@attributes']['AutoRenew'] }}</td>
                                @if($domain['@attributes']['WhoisGuard'] == "ENABLED")
                                  <td><a href="" class=" btn btn-success button-success-table">{{ $domain['@attributes']['WhoisGuard'] }}</a></td>
                                @else
                                <td><button class="bg-danger">{{ $domain['@attributes']['WhoisGuard'] }}</button></td>
                                @endif
                                <td class="update_dns">
                                  <button class="btn btn-info" data-toggle="modal" data-target="#UpdateDns-{{$domain['@attributes']['ID']}}" data-dns_value="{{$domain['@attributes']['Name']}}" data-dns_id="{{$domain['@attributes']['ID']}}">Update DNS</button>
                                
                                  <div class="modal fade" id="UpdateDns-{{$domain['@attributes']['ID']}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                    <div class="modal-dialog" role="document">
                                      <div class="modal-content">
                                        <div class="modal-header">
                                          <h5 class="modal-title" id="exampleModalLabel">Update DNS</h5>
                                          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                          </button>
                                        </div>
                                        <div class="modal-body">
                                            <form method="POST" action="{{route('admin.updateDNS')}}">
                                              @csrf
                                              <input type="hidden" name="domain_name" value="{{$domain['@attributes']['Name']}}">
                                              <input type="hidden" name="page" value="{{$currentPage}}">
                                              <div class="form-group">
                                                <label for="ns1">NS1:</label>
                                                <input type="text" name="ns1" class="form-control" id="ns1" maxlength="191" value="">
                                              </div>
                                              <div class="form-group">
                                                <label for="ns2">NS2:</label>
                                                <input type="text" name="ns2" class="form-control" id="ns2" maxlength="191" value="">
                                              </div>
                                              <div class="modal-footer">
                                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                <button type="submit" class="btn btn-primary">Update DNS</button>
                                              </div>
                                            </form>
                                          </div> <!-- end modal body-->
                                      </div><!-- end modal content-->
                                    </div>
                                  </div><!-- end modal-->
                                </td>
                                <td>{{ $domain['@attributes']['IsPremium'] }}</td>
                                <td>{{ $domain['@attributes']['IsOurDNS'] }}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div><!--col-->
        </div><!--row-->
        <div class="row">
            <div class="col-2">
                <div class="float-left">
                Total users {{$domains['CommandResponse']['Paging']['TotalItems']}} 
                </div>
            </div><!--col-->

            <div class="col-10">
                <div class="float-none">
                    <?php for($i=1;$i<=$paginate;$i++){ ?>
                        <a href="{{route('admin.index',[$i])}}">{{$i}}</a>
                    <?php } ?>
                </div>
            </div><!--col-->
        </div><!--row-->
    </div><!--card-body-->
</div><!--card-->
@endsection
@push('after-scripts')
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    
        <script>
            $(document).ready(function(){
                
                setTimeout(function() {
                    $(".alert-success").hide(); 
                }, 5000);

                $('tbody .update_dns button').on('click', function(){
                    var name = $(this).attr('data-dns_value');
                    var id = $(this).attr('data-dns_id');

                    $.ajax({
                        url : '/admin/getDNS',
                        type : 'GET',
                        data : {
                            'full_name' : name
                        },
                        dataType:'json',
                        success : function(data) {
                            $("#UpdateDns-"+id+" #ns1").val(data.domainNames.CommandResponse.DomainDNSGetListResult.Nameserver[0]);
                            $("#UpdateDns-"+id+" #ns2").val(data.domainNames.CommandResponse.DomainDNSGetListResult.Nameserver[1]);
                            // alert('Data: '+data);
                        },
                        error : function(request,error)
                        {
                            // alert("Request: "+JSON.stringify(request));
                        }
                    });

                });
            });
        </script>
        @endpush 
