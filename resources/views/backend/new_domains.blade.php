@extends('backend.layouts.app')

@section('title', app_name() . ' | ' . __('strings.backend.dashboard.title'))

@section('content')
    <style> 
        #domain-form {
            display: inline-block;
            width: 100%;
            text-align: right;
          }
          .input-outer{
            display: inline-block;
          }
    </style>
    <div class="row">
        <div class="col">
            <div class="card">
                <div class="card-header">
                    <strong>New Domains</strong>
                </div><!--card-header-->
                <div class="card-body">
                    <div class="row">
                        <div class="col-sm-5">
                            <h4 class="card-title mb-0">
                                All Domains
                            </h4>
                        </div><!--col-->
                        <div class="col-sm-7">
                            <form method="POST" action="{{route('admin.saveDomain')}}" id="domain-form">
                              @csrf
                              <div class="col-sm-5 form-group input-outer">
                                <input type="text" name="domain" class="form-control" id="create-domain" maxlength="191" value="" placeholder="Enter a domain name">
                              </div>
                              <button type="submit" class="btn btn-primary">Create</button>
                            </form> 
                        </div><!--col-->
                    </div><!--row-->
                    <div class="row mt-4">
                        <div class="col">
                            <div class="table-responsive">
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Domain Name</th>
                                            <th>Domain created At</th>
                                            <th>Error Name</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($domains as $key=>$domain)
                                            <tr>
                                                <td>{{$key+1}}</td>
                                                <td>{{ $domain->name }}</td>
                                                <td>{{ $domain->created_at }}</td>
                                                <td>{{ (isset($domain->errors[0]) && count($domain->errors)) ? $domain->errors[0]->name : "NA" }}</td>
                                                <td class="update_dns">
                                                <?php //if(isset($domain->errors[0]) && count($domain->errors)){ ?>
                                                  
                                                <?php // } else { ?>
                                                    <a href="{{route('admin.createCloudflare',['domain_name'=>$domain->name])}}" class="btn btn-info">Update DNS</a>
                                                <?php //} ?>
                                                </td>
                                            </tr>
                                        @endforeach 
                                    </tbody>
                                </table>
                            </div>
                        </div><!--col-->
                    </div><!--row-->
                    <div class="row">
                        <div class="col-2">
                            <div class="float-left">
                            Total users {{count($domains)}} 
                            </div>
                        </div><!--col-->

                        <div class="col-10">
                            <div class="float-none">
                                
                            </div>
                        </div><!--col-->
                    </div><!--row-->
                </div><!--card-body-->
                </div><!--card-body-->
            </div><!--card-->
        </div><!--col-->
    </div><!--row-->
@endsection
