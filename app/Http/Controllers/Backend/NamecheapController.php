<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\{Domain,Error};
use View;

class NamecheapController extends Controller
{
	private $apiKey;
	private $user;
	private $apiIp;
	private $organizationId;

	 public function __construct()
    {
        //blockio init
        $this->apiKey = env('namecheap_token');
        $this->user = env('namecheap_user');
        $this->apiIp = env('namecheap_ip');
        $this->organizationId = env('organization_id');
    }

    public function index($page=null)
    {
    	// $apiKey = env('namecheap_token');
    	// $user = env('namecheap_user');
    	// $apiIp = env('namecheap_ip');

    	$client = new \GuzzleHttp\Client(['http_errors' => false]);

    	$request = $client->get("https://api.namecheap.com/xml.response?ApiUser=". $this->user ."&ApiKey=". $this->apiKey ."&UserName=". $this->user ."&Page=". $page ."&Command=namecheap.domains.getList&ClientIp=". $this->apiIp);

		$xml = simplexml_load_string($request->getBody()->getContents());
		$json = json_encode($xml);
		$domains = json_decode($json,TRUE);
		$totalItems = $domains['CommandResponse']['Paging']['TotalItems'];
		$currentPage = $domains['CommandResponse']['Paging']['CurrentPage'];
		$pageSize = $domains['CommandResponse']['Paging']['PageSize'];
		
		$paginate = (int)ceil($totalItems/$pageSize);
		// dd($paginate);
    	return view('backend.namecheap.index',compact('domains','paginate','currentPage'));
    }

    public function namecheap(){

		dd('ion');
    }

    public function saveDomain(Request $request)
    {
    	$constantArray = config('constant.create_domain');
    	$client = new \GuzzleHttp\Client(['http_errors' => false]);
    	
    	$guzzleRequest = $client->get("https://api.namecheap.com/xml.response?ApiUser=". $this->user ."&ApiKey=". $this->apiKey ."&UserName=". $this->user ."&Command=namecheap.domains.create&ClientIp=". $this->apiIp ."&DomainName=". $request->domain ."&Years=". $constantArray['Years']."&AuxBillingFirstName=". $constantArray['AuxBillingFirstName']."&AuxBillingLastName=". $constantArray['AuxBillingLastName']."&AuxBillingAddress1=". $constantArray['AuxBillingAddress1']."&AuxBillingStateProvince=". $constantArray['AuxBillingStateProvince']."&AuxBillingPostalCode=". $constantArray['AuxBillingPostalCode']."&AuxBillingCountry=". $constantArray['AuxBillingCountry']."&AuxBillingPhone=". $constantArray['AuxBillingPhone']."&AuxBillingEmailAddress=". $constantArray['AuxBillingEmailAddress']."&AuxBillingCity=". $constantArray['AuxBillingCity']."&TechFirstName=". $constantArray['TechFirstName']."&TechLastName=". $constantArray['TechLastName']."&TechAddress1=". $constantArray['TechAddress1']."&TechStateProvince=". $constantArray['TechStateProvince']."&TechPostalCode=". $constantArray['TechPostalCode']."&TechCountry=". $constantArray['TechCountry']."&TechPhone=". $constantArray['TechPhone']."&TechEmailAddress=". $constantArray['TechEmailAddress']."&TechCity=". $constantArray['TechCity']."&AdminFirstName=". $constantArray['AdminFirstName']."&AdminLastName=". $constantArray['AdminLastName']."&AdminAddress1=". $constantArray['AdminAddress1']."&AdminStateProvince=". $constantArray['AdminStateProvince']."&AdminPostalCode=". $constantArray['AdminPostalCode']."&AdminCountry=". $constantArray['AdminCountry']."&AdminPhone=". $constantArray['AdminPhone']."&AdminEmailAddress=". $constantArray['AdminEmailAddress']."&AdminCity=". $constantArray['AdminCity']."&RegistrantFirstName=". $constantArray['RegistrantFirstName']."&RegistrantLastName=". $constantArray['RegistrantLastName']."&RegistrantAddress1=".$constantArray['RegistrantLastName']."&RegistrantStateProvince=". $constantArray['RegistrantStateProvince']."&RegistrantPostalCode=". $constantArray['RegistrantPostalCode']."&RegistrantCountry=". $constantArray['RegistrantCountry']."&RegistrantPhone=". $constantArray['RegistrantPhone']."&RegistrantEmailAddress=". $constantArray['RegistrantEmailAddress']."&RegistrantCity=". $constantArray['RegistrantCity']."&AddFreeWhoisguard=". $constantArray['AddFreeWhoisguard']."&WGEnabled=". $constantArray['WGEnabled']."&GenerateAdminOrderRefId=". $constantArray['GenerateAdminOrderRefId']."&Nameservers=". $constantArray['Nameservers']
    	);
    	
    	$xml = simplexml_load_string($guzzleRequest->getBody()->getContents());
		$json = json_encode($xml);
		$response = json_decode($json,TRUE);

		if(count($response['Errors'])){
			$domain = new Domain;
			$domain->name = $response['CommandResponse']['DomainCreateResult']['@attributes']['Domain'];
			$domain->stage = config('constant.inverse_domain_stages.namecheap');
			$domain->save();

			if($domain->save()){
				$error = new Error;
				$error->domain_id = $domain->id;
				$error->name = $response['Errors']['Error'];
				$error->type = config('constant.inverse_domain_stages.namecheap');
				$error->save();
			}

			return redirect()->back()->withFlashDanger($response['Errors']['Error']);
		}
		// metando.club
		$domain = new Domain;
		$domain->name = $response['CommandResponse']['DomainCreateResult']['@attributes']['Domain'];
		$domain->stage = config('constant.inverse_domain_stages.cloudflare');
		$domain->save();

    	return redirect()->route('admin.index')->withFlashSuccess('Domain name created successfully.');
    }

    public function getDNS(Request $request)
    {
    	$nameArray = explode('.',$request->full_name);
    	$firstName = $nameArray[0];
    	$lastName = end($nameArray);

    	$client = new \GuzzleHttp\Client(['http_errors' => false]);

    	$guzzleRequest = $client->get("https://api.namecheap.com/xml.response?ApiUser=". $this->user ."&ApiKey=". $this->apiKey ."&UserName=". $this->user ."&Command=namecheap.domains.dns.getList&ClientIp=". $this->apiIp ."&SLD=". $firstName ."&TLD=".$lastName);
    	
    	$xml = simplexml_load_string($guzzleRequest->getBody()->getContents());
		$json = json_encode($xml);
		$domainNames = json_decode($json,TRUE);
		if(count($domainNames['Errors'])){
			
			return response()->json(['domainNames'=>$domainNames],500);
		}
    	
    	return response()->json(['domainNames'=>$domainNames],200);		
    }

    public function updateDNS(Request $request){
    	// dd($request->all());
    	$nameArray = explode('.',$request->domain_name);
    	$firstName = $nameArray[0];
    	$lastName = end($nameArray);
    	$page = $request->page;
    	$client = new \GuzzleHttp\Client(['http_errors' => false]);

    	$guzzleRequest = $client->get("https://api.namecheap.com/xml.response?ApiUser=". $this->user ."&ApiKey=". $this->apiKey ."&UserName=". $this->user ."&Command=namecheap.domains.dns.setCustom&ClientIp=". $this->apiIp ."&SLD=". $firstName ."&TLD=".$lastName."&NameServers=".$request->ns1.','.$request->ns2);
    	
    	$xml = simplexml_load_string($guzzleRequest->getBody()->getContents());
		$json = json_encode($xml);
		$setCustom = json_decode($json,TRUE);

		if(count($setCustom['Errors'])){

			return redirect()->route('admin.index',compact('page'))->withFlashDanger('Something went worng.');
		}

		return redirect()->route('admin.index',compact('page'))->withFlashSuccess('DNS updated successfully.');
    }
    public function createCloudflare(Request $request)
    {
    	$orgId = env('organization_id');
    	$headers = [
			'headers' => [
		        'X-Auth-Email' => env('x_auth_email'),
		        'X-Auth-Key'   => env('x_auth_key'),
		        'Content-Type' => 'application/json'
		    ],'http_errors' => false
    	];
    	//zone or site body
 		$options = [
 			'json' => [
 				'name'=>$request->domain_name,
 				'jump_start'=>true,
 				'account'=>[
 					'id'=>$orgId
 				]
 			]
 		];
 		//api for create a zone or site on cloudflare
    	$client = new \GuzzleHttp\Client($headers);
    	$guzzleRequest = $client->post("https://api.cloudflare.com/client/v4/zones",$options);
    	$json = $guzzleRequest->getBody()->getContents();
		$dnsDetails = json_decode($json,TRUE);


		if($dnsDetails['success'] == "true"){
			$dnsNames = [$request->domain_name, "www"];
			foreach ($dnsNames as $key => $value) {
				//name server api data
				$dnsValues = [
					'json'=> [
						"type"		=>"A",
						"name"		=>$value,
						"content"	=>"45.76.133.204",
						"priority"	=>"10",
						"proxied"	=>true
					]
				];
				//api for points dns to name servers 
				$zoneClient = new \GuzzleHttp\Client($headers);
				$guzzleRequest = $zoneClient->post("https://api.cloudflare.com/client/v4/zones/". $dnsDetails['result']['id'] ."/dns_records",$dnsValues);
			}// end foreach
			//ipv6 api data
			$ipvData = [
				'json' =>[
					"value"=>"off"
				]
			];
			//Api for off the Ipv6 compatibility
			$ipvClient = new \GuzzleHttp\Client($headers);
			$guzzleRequest = $ipvClient->patch("https://api.cloudflare.com/client/v4/zones/". $dnsDetails['result']['id'] ."/settings/ipv6",$ipvData);
			$this->__updateDomainStage($request->domain_name, config('constant.inverse_domain_stages.plesk'));
			
			return redirect()->route('admin.newDomains')->withFlashSuccess('Domain created successfully on cloudflare');
		}

		return redirect()->back()->withFlashDanger($dnsDetails['errors'][0]['message']);
    }

    public function newDomains()
    {
    	$domains = Domain::with(['errors'=>function($error){
    		$error->orderBy('created_at','desc')->first();
    	}])->get();

    	return view('backend.new_domains',compact('domains'));
    }

    public function createPleskDomain(Request $request)
    {
    	$headers = [
			'headers' => [
		        'Content-Type' => 'application/xml',
		        "HTTP_AUTH_LOGIN" => "admin",
				"HTTP_AUTH_PASSWD" => "!4W97pGvzqwlnpfZ"
		    ],
		    'http_errors' => false,
		    'verify' => false
    	];
    	$xmlValues ="<packet><site><add><gen_setup><name>". $request->domain_name ."</name><webspace-id>45</webspace-id></gen_setup><hosting><vrt_hst><property><name>fp</name><value>false</value></property><property><name>fp_ssl</name><value>false</value></property><property><name>fp_auth</name><value>false</value></property><property><name>". $request->domain_name ."</name><value/></property><property><name>ajskdlf1</name><value/></property><property><name>ssl</name><value>false</value></property><property><name>php</name><value>true</value></property><property><name>ssi</name><value>false</value></property><property><name>cgi</name><value>false</value></property><property><name>asp</name><value>false</value></property><property><name>webstat</name><value>awstats</value></property><property><name>webstat_protected</name><value>false</value></property><property><name>errdocs</name><value>false</value></property><property><name>fastcgi</name><value>false</value></property><property><name>cgi_mode</name><value>webspace</value></property><property><name>www_root</name><value>/". $request->domain_name ."</value></property><property><name>safe_mode</name><value>off</value></property><property><name>open_basedir</name><value>{WEBSPACEROOT}{/}{:}{TMP}{/}</value></property></vrt_hst></hosting></add></site></packet>";
    	$client = new \GuzzleHttp\Client($headers);
    	$xmlRequest = $client->post("https://45.76.133.204:8443/enterprise/control/agent.php",['body'=>$xmlValues]);

    	$xml = simplexml_load_string($xmlRequest->getBody()->getContents());
		$json = json_encode($xml);
		$response = json_decode($json,TRUE);

		if($response['site']['add']['result']['status'] == "error"){
			return redirect()->back()->withFlashDanger($response['site']['add']['result']['errtext']);
		}
		$this->__updateDomainStage($request->domain_name, config('constant.inverse_domain_stages.armor'));

		return redirect()->route('admin.newDomains')
			->withFlashSuccess('Website domain added successfully to Plesk.');
    }

    private function __updateDomainStage($domainName, $stage){
		Domain::where('name',$domainName)->update(['stage'=>$stage]);
		return true;
    }

    public function createTrafficArmorCampaign(){
    	$headers = [
			'headers' => [
		        "TA-API-Key" 	=> "a3a7bdce4f61cad375a00ee41b67cab9",
		        'Content-Type' 	=> 'application/x-www-form-urlencoded'
		    ],
		    'http_errors' => false
    	];

	    $getCampaign = new \GuzzleHttp\Client($headers);
		$xmlRequest = $getCampaign->post("https://trafficarmor.com/campaigns?page=1");
    	// $html = $xmlRequest->getBody()->getContents();

    	$dom = new \domDocument();
    	@$dom->loadHTML($xmlRequest->getBody()->getContents());

    	$dom->saveHTML();

    	// $tables = $dom->getElementsByTagName('table');
    	

    	$firstTable = $dom->getElementsByTagName('table')->item(0);

		// iterate over each row in the table
		foreach($firstTable->getElementsByTagName('tr') as $tr)
		{
		    $tds = $tr->getElementsByTagName('td'); // get the columns in this row
		    $campaignId = $tds->item(1)->nodeValue;
		    echo $campaignId;
		    echo "<br>";
		    var_dump($campaignId);
		    echo "<br>";
		    print_r($campaignId);
		    die;
		}

    	dd($tables);


		echo '<pre>';
		print_r($table);
		echo '</pre>';
		dd('1');
    	dd($html_entity_decode);
    // 	$client = new \GuzzleHttp\Client($headers);
    // 	$xmlRequest = $client->post("https://trafficarmor.com/campaigns/edit",[
    // 			'form_params' =>[
				// 	'label'=>'test new',
				// 	'integration_method'=>'PHP',
				// 	'sticky_cloaking'=>0,
				// 	'global_dbs'=>[1,2,5,12,13,16],
				// 	'cloak_global_db_cities'=>1,
				// 	'hybrid_mode'=>1,
				// 	'real_url_1'=>'https://www.unfiltered.com',
				// 	'safe_redirect_url'=>'https://www.filtered.com',
				// 	'uncloaking_action'=>'iframe',
				// 	'cloaking_action'=>'paste_html',
				// 	'cloak_uncommon_isps'=>1,
				// 	'cloak_proxies'=>1,
				// 	'cloak_headless_browsers'=>1,
				// 	'strip_ref'=>1
				// ]
    // 		]);
  //   	if (strpos($html, 'NEW INTEGRATION') !== false) {
		// }
  //   	dd($html);


    	// $html = view('backend.traffic_armor_response');
    	// dd($html->render());
    	// print_r($html);
  //   	$strReplace = str_replace("\n", '', $html)
  //   	$doc = new \DOMDocument();
		// $doc1 = $doc->loadHTML($doc);
		// dd($doc1);
    }

    public function updateFilesToPlesk(){
    	// dd('in');
    	// dd(asset('storage/uploads_plesk_files/bootloader.php'));
    	// dd(fopen(asset('storage/uploads_plesk_files/bootloader.php'), "r"));
    	// dd(file_get_contents(asset('storage/uploads_plesk_files/bootloader.php')));

    	$url = "https://45.76.133.204:8443/enterprise/control/agent.php";

		// $localFile = asset('storage/uploads_plesk_files/test260419.php');
		$localFile = asset('storage/uploads_plesk_files/test260419.php');
		$headers = array(
			"HTTP_AUTH_LOGIN: admin",
			"HTTP_AUTH_PASSWD: !4W97pGvzqwlnpfZ",
			"HTTP_PRETTY_PRINT: TRUE",
			"Content-Type: multipart/form-data",
		);
		 
		// Initialize the curl engine
		$ch = curl_init();
		 
		// Set the curl options
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
		// this line makes it work under https
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		// Set the URL to be processed
		curl_setopt($ch, CURLOPT_URL, $url);
		 
		curl_setopt($ch, CURLOPT_POSTFIELDS, array
		('sampfile'=>"@$localFile"));
		 
		$result = curl_exec($ch);
		 
		if (curl_errno($ch)) {
		echo "\n\n-------------------------\n" .
		"cURL error number:" .
		curl_errno($ch);
		echo "\n\ncURL error:" . curl_error($ch);
		}
		 
		curl_close($ch);




		return;
    }
}
