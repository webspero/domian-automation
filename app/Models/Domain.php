<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Domain extends Model
{
    public function errors()
    {
		return $this->hasMany('App\Models\Error');
	}
}
