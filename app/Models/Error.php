<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Error extends Model
{
    public function domain()
    {
    	return $this->belongsTo('App\Models\Domain');
    }
}
